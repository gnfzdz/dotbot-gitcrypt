import os
import dotbot

from dotbot.messenger import Messenger

def check_path_decryption_status(path):
    if os.path.isdir(path):
        for subdir, dirs, files in os.walk(path):
            for f in files:
                if not is_file_decrypted(os.path.join(subdir, f)):
                    return False
        return True
    else:
        return is_file_decrypted(path)

def is_file_decrypted(path):
    header = ""
    with open(path, "rb") as f:
        header = f.read(9)
    return b"\x00GITCRYPT" not in header

def get_full_path(basedir, path):
    return path if os.path.isabs(path) else os.path.join(basedir, path)

def find_plugin(context, directive):
    try:
        plugins = (p(context) for p in dotbot.Plugin.__subclasses__())
        return next(filter(lambda p: p.can_handle(directive), plugins))
    except:
        Messenger().debug("Failed to find plugin for %s" % directive)
        return None

try:
    from dotbot_conditional import Condition

    class GitCryptCondition(Condition):

        """
        Condition testing if a path and/or all contained files are not presently encryped with git-crypt
        """

        _directive = "git-decrypted"

        def can_handle(self, directive):
            return directive == self._directive

        def handle(self, directive, data):
            if not self.can_handle(directive):
                raise ValueError("%s cannot handle directive %s" % (self.__class__.__name__, directive))

            return self._check_decryption_status(data)

        def _check_decryption_status(self, data):
            basedir = self._context.base_directory()
            if data is None:
                return check_path_decryption_status(basedir)
            elif isinstance(data, str):
                return check_path_decryption_status(get_full_path(basedir, data))
            elif isinstance(data, list):
                for path in data:
                    if not check_path_decryption_status(get_full_path(basedir, path)):
                        return False
                return True
            else:
                raise ValueError("%s provided value must be one of: None, string, list" % self.__class__.__name__)
except:
    Messenger().debug("Unable to load GitCryptCondition for dotbot_conditional")

import dotbot

class CryptSync(dotbot.Plugin):

    '''
    Wrapper for dotbot-sync that first checks if the source path is still encrypted by git-crypt before proceeding to rsync.
    '''

    _directive = 'cryptsync'
    _sync_directive = 'sync'
    _sync_plugin = None

    def can_handle(self, directive):
        return directive == self._directive and self._get_sync_plugin() is not None

    def handle(self, directive, data):
        if not self.can_handle(directive):
            raise ValueError("%s cannot handle directive %s" % (self.__class__.__name__, directive))
        return self._process_cryptsync(data)

    def _get_sync_plugin(self):
        if self._sync_plugin is None:
            self._sync_plugin = find_plugin(self._context, self._sync_directive)
        return self._sync_plugin

    def _process_cryptsync(self, data):
        filtered = self._filter_syncdata(data)
        return self._get_sync_plugin().handle(self._sync_directive, filtered)

    def _filter_syncdata(self, data):
        syncdata = {}
        for key, value in data.items():
            if value is None or 'path' not in value or value['path'] is None:
                self._log.warning('Skipping sync for %s due to missing property "path"' % (key))
                continue
            elif check_path_decryption_status(os.path.join(self._context.base_directory(), value['path'])):
                syncdata[key] = value
            else:
                self._log.debug('Skipping sync for %s after validating encrypted' % value['path'])
                continue
        return syncdata

class CryptInclude(dotbot.Plugin):

    '''
    Wrapper for dotbot-include that first checks if the target include is still encrypted by git-crypt before proceeding to include.
    '''

    _directive = 'cryptinclude'
    _include_directive = 'include'
    _include_plugin = None

    def can_handle(self, directive):
        return directive == self._directive and self._get_include_plugin() is not None

    def handle(self, directive, data):
        if not self.can_handle(directive):
            raise ValueError("%s cannot handle directive %s" % (self.__class__.__name__, directive))
        return self._process_cryptinclude(data)

    def _get_include_plugin(self):
        if self._include_plugin is None:
            self._include_plugin = find_plugin(self._context, self._include_directive)
        return self._include_plugin

    def _process_cryptinclude(self, path):
        fullpath = os.path.join(self._context.base_directory(), path)
        if check_path_decryption_status(fullpath):
            return self._include_plugin.handle(self._include_directive, path)
        else:
            self._log.debug("Skipping encrypted include %s" % path)
            return True
