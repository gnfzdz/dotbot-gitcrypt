# dotbot-gitcrypt

Provides plugins supporting the use of [dotbot](https://github.com/anishathalye/dotbot) with [git-crypt](https://github.com/AGWA/git-crypt):
- cryptsync: a simple wrapper for [dotbot-sync](https://github.com/j1r1k/dotbot-sync) that validates the source path is decrypted before executing the sync
- cryptinclude: a simple wrapper for [dotbot-include](https://gitlab.com/gnfzdz/dotbot-include) that validates the source path is decrypted before attempting to include
- git-decrypted: a condition for use with [dotbot-conditional](https://gitlab.com/gnfzdz/dotbot-conditional) allowing conditional task executing based on whether targeted path(s) are decrypted

## Installation

The below examples assume plugins are installed to a submodule directory, but feel free to manage as you see fit

1. Add the plugin as a submodule to your dotfiles repository.
   `git submodule add https://gitlab.com/gnfzdz/dotbot-gitcrypt.git meta/dotbot-gitcrypt`

2. (Optional) Add the dotbot-conditional plugin as a submodule to your dotfiles repository if the git-decrypted condition will be used
   `git submodule add https://gitlab.com/gnfzdz/dotbot-conditional.git meta/dotbot-conditional`

3. (Optional) Add the dotbot-sync plugin as a submodule to your dotfiles repository if cryptsync will be used
   `git submodule add https://github.com/j1r1k/dotbot-sync.git meta/dotbot-sync`
 
4. (Optional) Add the dotbot-include plugin as a submodule to your dotfiles repository if cryptinclude will be used
   `git submodule add https://gitlab.com/gnfzdz/dotbot-include.git meta/dotbot-include
 
5. Register the plugins with cli arguments when executing dotbot. NOTE: dotbot-conditional must be added before dotbot-gitcrypt
   `... --plugin-dir meta/dotbot-conditional --plugin meta/dotbot-sync/sync.py --plugin meta/dotbot-include/include.py --plugin meta/dotbot-gitcrypt/gitcrypt.py ...`

## Cryptsync

This plugin is a simple wrapper around the dotbot-sync plugin. It validates the source path is not encrypted before delegating to dotbot-sync. See [dotbot-sync](https://github.com/j1r1k/dotbot-sync) for explanation of the contract.

```yaml

# Assuming two files to be copied:
# - ssh/authorized_keys   managed by git-crypt
# - .bash_profile         not managed by git-crypt

# when git-crypt is unlocked, both files will be copied to their target location
- cryptsync:
  ~/.ssh/authorized_keys:
    path: 'ssh/authorized_keys'
    fmode: 400 
  ~/.bash_profile:
    path: 'bash/bash_profile'
    fmode: 600

# when git-crypt is locked (and ssh/authorized_keys is encrypted), only the .bash_profile will be copied
- cryptsync:
  ~/.ssh/authorized_keys:
    path: 'ssh/authorized_keys'
    fmode: 400 
  ~/.bash_profile:
    path: 'bash/bash_profile'
    fmode: 600

```

## Cryptinclude

This plugin is a simple wrapper around the dotbot-include plugin. It validates the target configuration file is not encrypted before proceeding with the include. See [dotbot-include](https://gitlab.com/gnfzdz/dotbot-include.git)

```yaml
- cryptinclude: meta/config/secret_setup.conf.yaml
```

## GitDecryptedCondition

This plugin provides a condition for use with [dotbot-conditional](https://gitlab.com/gnfzdz/dotbot-conditional.git). It returns true only if all target paths are not currently encrypted with git-crypt. In the case of directories, the plugin will check recursively within the directories and only return true if all contained files are not currently encrypted with git-crypt.


```yaml
# In the below examples, all files in the subdirectory ssh are managed by git-crypt

# If no value is passed to the git-crypt directive, basedir configured with dotbot is searched recursively ensuring no files are locked
- conditional:
    if:
      git-decrypted:

# This will check that a single file is not encrypted
- conditional:
    if:
      git-decrypted: ssh/id_rsa

# If multiple files are provided, the condition will only return true if none of the files are encrypted
- conditional:
    if:
      git-decrypted:
      - ssh/id_rsa
      - ssh/config

# In the below examples, the condition will only return true if all files within the directory ssh are not encrypted
- conditional:
    if:
      git-decrypted:
      - ssh
    then:
    - create:
        ~/.ssh:
        mode: 0700
    - sync:
        ~/.ssh:
          path: ssh
          fmode: 400
```
